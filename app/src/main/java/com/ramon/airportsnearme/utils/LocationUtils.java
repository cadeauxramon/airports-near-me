package com.ramon.airportsnearme.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.ramon.airportsnearme.R;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

import timber.log.Timber;

public class LocationUtils {

    /*
    * Attempts to get an Address from a String that the user provides
     *
     */

    @Nullable
    public static Address geocodeAddress(Geocoder geocoder, String location) {
        List<Address> list = null;
        try {
            list = geocoder.getFromLocationName(location, 1);
        } catch (Exception e) {
            Timber.e(e);
        }
        if (null != list && !list.isEmpty()) {
            Timber.d("Got an address");
            return list.get(0);
        }

        Timber.d("returning Null");
        return null;
    }

    /*
     * Attempts to format an Address to a String that resembles the format of
     * address,city,state,zip
     */

    public static String formatAddress(Address address) {
        String strAddress;
        StringBuilder stringBuilder = new StringBuilder();

        if (address.getLocality() != null && !address.getLocality().equals("")) {
            stringBuilder.append(address.getLocality());
        }

        if (address.getAdminArea() != null && !address.getAdminArea().equals("")) {
            if (stringBuilder.length() > 0) stringBuilder.append(", ");
            stringBuilder.append(address.getAdminArea());
        }


        if (address.getPostalCode() != null && !address.getPostalCode().equals("")) {
            if (stringBuilder.length() > 0) stringBuilder.append(", ");
            stringBuilder.append(address.getAdminArea());
        }

        if (stringBuilder.length() == 0) {
            for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                if (stringBuilder.length() > 0) {
                    stringBuilder.append(", ");
                }
                stringBuilder.append(address.getAddressLine(i));
            }
        }

        strAddress = stringBuilder.toString();
        if (strAddress.equals("null")) strAddress = "";

        return strAddress;
    }


    //hack around generating he city state , zip format for list view
    static String generateCityStateZip(String city, String state, String country) {
        StringBuilder stringBuilder= new StringBuilder();
        if (!StringUtils.isEmpty(city)){
         stringBuilder.append(city).append(",");
        }

        if (!StringUtils.isEmpty(state)){
            stringBuilder.append(state).append(",");

        }

        if (!StringUtils.isEmpty(country)){
            stringBuilder.append(country);

        }

        return stringBuilder.toString();
    }

    //check to ensure we have permissions
    public static boolean haveLocationPermissions(Activity activity) {
        boolean haveCoarse = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        boolean haveFine = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;

        return haveCoarse && haveFine;
    }


}
