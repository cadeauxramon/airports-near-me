package com.ramon.airportsnearme.utils;

import android.view.View;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.ramon.airportsnearme.webservice.api.station.Station;

import org.apache.commons.lang3.StringUtils;

public class BindingAdapters {

    //custom view binding to show and hide xml elements
    @BindingAdapter("visibleGone")
    public static void showHide(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    //custom view binding to show and hide xml elements

    @BindingAdapter("formatText")
    public static void formatText(View view, Station station) {
        if (view instanceof TextView && station != null) {
            ((TextView) view).setText(LocationUtils.generateCityStateZip(station.getCity(), station.getState().getName(), station.getCountry().getName()));
            return;
        }
        view.setVisibility(View.GONE);
    }

    //custom view binding to show and hide xml elements

    @BindingAdapter("hideFieldIfNoText")
    public static void hideFieldIfNoText(View view, String station) {
       if (!StringUtils.isEmpty(station) && !station.equals("0.0")){
           showHide(view,true);
       }else {
           showHide(view,false);
       }
    }

    //custom view binding to convert and show Altitude data

    @BindingAdapter("formatBarometerText")
    public static void formatBarometerText(View view, Double barometer) {
        String string = Utils.convertFeetToInches(barometer) + " Hg";
        ((TextView) view).setText(string);
    }

    //custom view binding to make date human readable

    @BindingAdapter("formatDateText")
    public static void formatDateText(View view, String observedDate) {
        ((TextView) view).setText(Utils.changeDateFormat("yyyy-MM-dd'T'HH:mm:s.S'Z'", observedDate, "MMMM dd yyyy hh: mm a"));
    }

    //custom view binding to show data and their corresponding units

    @BindingAdapter({"bind:data", "bind:units"})
    public static void appendUnits(View view, String data, String units) {
        data += units;
        ((TextView) view).setText(data);
    }
}
