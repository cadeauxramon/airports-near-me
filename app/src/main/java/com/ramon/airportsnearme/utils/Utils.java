package com.ramon.airportsnearme.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.util.TypedValue;

import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;

import com.ramon.airportsnearme.BuildConfig;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utils {

    //sends a phone number to the dialer and pre fills the data
    public static void dispatchCallIntent(Context context, String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        context.startActivity(intent);
    }

    //hack to quickly make strings
    public static String generateString(String... strings) {
        StringBuilder result = new StringBuilder();
        for (String s : strings) {
            result.append(s);
        }
        return result.length() > 0 ? result.substring(0, result.length()) : "";
    }

    //hack to quickly turn dp to px for the layout custom fixes that are done
    public static int dpToPx(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics());
    }


    //hack to get bitmaps from Vectors
    public static Bitmap getBitmapFromVectorDrawable(Context context, int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(context, drawableId);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            drawable = (DrawableCompat.wrap(drawable)).mutate();
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /*
        https://www.convertunits.com/from/inches+Hg/to/feet+of+air

        1 inches mercury = 876.37452845287 feet of air
     */

    public static String convertFeetToInches(Double barometer) {
        return barometer != null ? String.valueOf(barometer / 876.37452845287) : "";
    }

    /**
     *  converts a date string to another custom format
     *
     * @param oldFormat- start format ex Isodate
     * @param date - date string tha was returned
     * @param newPattern
     * @return
     */
    public static String changeDateFormat(String oldFormat,String date, String newPattern){
        String dateForService = "";
        try {
            SimpleDateFormat simpleDateFormat= new SimpleDateFormat(oldFormat, Locale.US);
            Date oldDate = simpleDateFormat.parse(date);
            SimpleDateFormat newFormat = new SimpleDateFormat(newPattern, Locale.US);
            dateForService = newFormat.format(oldDate);
        } catch (ParseException e) {
            if (BuildConfig.LOGGING_ENABLED) {
                e.printStackTrace();
            }
        }

        return dateForService;
    }
}
