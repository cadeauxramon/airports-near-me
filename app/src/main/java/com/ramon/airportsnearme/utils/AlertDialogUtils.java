package com.ramon.airportsnearme.utils;

import android.app.Activity;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import org.apache.commons.lang3.StringUtils;

/**
 * initally was planning on using Alert Dialogs but this approach was not needed
 */

public class AlertDialogUtils {

     static AlertDialog.Builder createAlertDialog(Activity activity, String title, String message, String postiveButton, DialogInterface.OnClickListener positiveAction
            , String negativeButton, DialogInterface.OnClickListener negativeAction) {

        AlertDialog.Builder builder= new AlertDialog.Builder(activity);
        if (StringUtils.isNotEmpty(title)){
            builder.setTitle(title);
        } if (StringUtils.isNotEmpty(message)){
            builder.setMessage(message);
        } if (StringUtils.isNotEmpty(postiveButton)){
            builder.setPositiveButton(postiveButton,positiveAction);
        } if (StringUtils.isNotEmpty(negativeButton)){
            builder.setNegativeButton(negativeButton,negativeAction);
        }

        return builder;

    }

    public static AlertDialog.Builder createSuccessDialog(Activity activity, String title, String message, String positiveButton, DialogInterface.OnClickListener onClickListener) {
        return createAlertDialog(activity,title,message,positiveButton,onClickListener,null,null);
    }

    public static AlertDialog.Builder UnknownErrorDialog(Activity activity) {
        return createAlertDialog(activity,null,"Something Unknown Happened","Ok",(dialog, which) -> {
            dialog.dismiss();
        },null,null);
    }
}
