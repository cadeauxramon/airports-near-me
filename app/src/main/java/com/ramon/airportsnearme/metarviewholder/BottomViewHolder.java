package com.ramon.airportsnearme.metarviewholder;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.ramon.airportsnearme.R;
import com.ramon.airportsnearme.databinding.BottomViewHolderBinding;
import com.ramon.airportsnearme.webservice.api.decoded.WeatherData;

public class BottomViewHolder extends RecyclerView.ViewHolder {

    private final BottomViewHolderBinding binding;

    private BottomViewHolder(@NonNull BottomViewHolderBinding itemView) {
        super(itemView.getRoot());
        this.binding = itemView;
    }

    public static RecyclerView.ViewHolder inflate(ViewGroup parent) {
        BottomViewHolderBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.bottom_view_holder,
                        parent, false);


        return new BottomViewHolder(binding);
    }

    public void bind(WeatherData weatherdata) {
        binding.setWeather(weatherdata);
        binding.executePendingBindings();
    }
}
