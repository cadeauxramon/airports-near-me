package com.ramon.airportsnearme.metarviewholder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.ramon.airportsnearme.R;
import com.ramon.airportsnearme.databinding.MetarTopDetailsBinding;
import com.ramon.airportsnearme.webservice.api.decoded.WeatherData;

public class MetarTopViewHolder extends RecyclerView.ViewHolder {

    private final MetarTopDetailsBinding binding;

     private MetarTopViewHolder(@NonNull MetarTopDetailsBinding itemView) {
        super(itemView.getRoot());
        this.binding=itemView;
    }

    public static RecyclerView.ViewHolder inflate(ViewGroup parent) {
        MetarTopDetailsBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.metar_top_details,
                        parent, false);


        return new MetarTopViewHolder(binding);
    }

    public void bind(String rawString, WeatherData weatherdata) {
        binding.setRaw(rawString);
        binding.setWeather(weatherdata);
        binding.executePendingBindings();
    }
}
