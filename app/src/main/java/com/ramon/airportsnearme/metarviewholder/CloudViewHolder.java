package com.ramon.airportsnearme.metarviewholder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.ramon.airportsnearme.R;
import com.ramon.airportsnearme.databinding.CloudViewHolderBinding;
import com.ramon.airportsnearme.webservice.api.decoded.Cloud;


public class CloudViewHolder extends RecyclerView.ViewHolder {

    private final CloudViewHolderBinding binding;

    private CloudViewHolder(@NonNull CloudViewHolderBinding itemView) {
        super(itemView.getRoot());
        this.binding= itemView;
    }

    public static RecyclerView.ViewHolder inflate(ViewGroup parent) {
        CloudViewHolderBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.cloud_view_holder,
                        parent, false);


        return new CloudViewHolder(binding);
    }

    public void bind(Cloud cloud) {
        binding.setCloud(cloud);
        binding.executePendingBindings();
    }
}
