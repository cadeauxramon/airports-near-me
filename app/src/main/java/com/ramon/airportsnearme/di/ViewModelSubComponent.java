package com.ramon.airportsnearme.di;


import com.ramon.airportsnearme.landingfragment.LandingViewModel;
import com.ramon.airportsnearme.locationdetails.LocationDetailsViewModel;

import dagger.Subcomponent;

/**
 * A sub component to create ViewModels.
 */
@Subcomponent
public interface ViewModelSubComponent {
    LandingViewModel landingViewModel();

    LocationDetailsViewModel detailsViewModel();

    @Subcomponent.Builder
    interface Builder {
        ViewModelSubComponent build();
    }

}
