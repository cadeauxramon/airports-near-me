package com.ramon.airportsnearme.di;

import android.app.Application;

import androidx.lifecycle.ViewModelProvider;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.ramon.airportsnearme.webservice.api.ApiClient;
import com.ramon.airportsnearme.webservice.api.ApiEndpoint;
import com.ramon.airportsnearme.webservice.api.ApiInterface;
import com.ramon.airportsnearme.webservice.configuration.OkHttpManagerConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(subcomponents = ViewModelSubComponent.class)
class AppModule {

    @Provides
    @Singleton
    public ApiClient provideApiClient(Application app, ApiInterface client) {
        return new ApiClient(app, client);
    }

    @Provides
    @Singleton
    public ApiInterface provideApiInterface(OkHttpManagerConfig okHttpManager, GsonConverterFactory factory, ApiEndpoint endpoint) {
        return new Retrofit.Builder()
                .client(okHttpManager.getOkHttpClient())
                .addConverterFactory(factory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(endpoint.getEndpoint())
                .build()
                .create(ApiInterface.class);
    }

    @Provides
    public FusedLocationProviderClient fusedLocationProviderClient(Application application){
        return new FusedLocationProviderClient(application.getBaseContext());
    }

    @Singleton
    @Provides
    ViewModelProvider.Factory provideViewModelFactory(ViewModelSubComponent.Builder viewModelSubComponent) {

        return new AppModelFactory(viewModelSubComponent.build());
    }
}
