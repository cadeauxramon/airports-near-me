package com.ramon.airportsnearme.di;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.ramon.airportsnearme.BaseApplication;

import org.jetbrains.annotations.NotNull;

import dagger.android.AndroidInjection;
import dagger.android.support.AndroidSupportInjection;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * AppInjector is a helper class to automatically inject fragments if they implement {@link Injectable}.
 */
public class AppInjector {
    private AppInjector() {
    }

    public static void init(BaseApplication baseApplication) {
        DaggerAppComponent.builder().application(baseApplication)
                .build().inject(baseApplication);

        baseApplication
                .registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                    @Override
                    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                        handleActivity(activity);
                    }

                    @Override
                    public void onActivityStarted(@NotNull Activity activity) {

                    }

                    @Override
                    public void onActivityResumed(@NotNull Activity activity) {

                    }

                    @Override
                    public void onActivityPaused(@NotNull Activity activity) {

                    }

                    @Override
                    public void onActivityStopped(@NotNull Activity activity) {

                    }

                    @Override
                    public void onActivitySaveInstanceState(@NotNull Activity activity, Bundle outState) {

                    }

                    @Override
                    public void onActivityDestroyed(@NotNull Activity activity) {

                    }
                });
    }

    private static void handleActivity(Activity activity) {
        if (activity instanceof HasSupportFragmentInjector) {
            AndroidInjection.inject(activity);
        }
        if (activity instanceof FragmentActivity) {
            ((FragmentActivity) activity).getSupportFragmentManager()
                    .registerFragmentLifecycleCallbacks(
                            new FragmentManager.FragmentLifecycleCallbacks() {
                                @Override
                                public void onFragmentCreated(@NotNull FragmentManager fm, @NotNull Fragment fragment,
                                                              Bundle savedInstanceState) {
                                    if (fragment instanceof Injectable) {
                                        AndroidSupportInjection.inject(fragment);
                                    }
                                }
                            }, true);
        }
    }
}
