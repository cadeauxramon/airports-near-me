package com.ramon.airportsnearme.di;


import com.ramon.airportsnearme.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Class to provide Activities Dagger
 * */
@Module
public abstract class MainActivityModule {
    @ContributesAndroidInjector(modules = FragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();
}
