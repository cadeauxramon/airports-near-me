package com.ramon.airportsnearme.di;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.preference.PreferenceManager;

import com.google.gson.Gson;
import com.ramon.airportsnearme.webservice.api.ApiEndpoint;
import com.ramon.airportsnearme.webservice.configuration.InterceptorConfig;
import com.ramon.airportsnearme.webservice.configuration.OkHttpManagerConfig;
import com.ramon.airportsnearme.webservice.configuration.RequestTokenInterceptor;
import com.ramon.airportsnearme.webservice.configuration.UnsafeHostnameVerifierConfig;
import com.ramon.airportsnearme.webservice.configuration.UnsafeTrustManagerConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.CertificatePinner;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class InjectionModule {

    @Provides
    @Singleton
    public ApiEndpoint providesEndpoint() {
        return new ApiEndpoint();
    }

    @Provides
    @Singleton
    public GsonConverterFactory provideConverterFactory() {
        return GsonConverterFactory.create();
    }


    @Provides
    @Singleton
    public OkHttpManagerConfig provideSolidOkHttpManager(UnsafeTrustManagerConfig unsafeTrustManager, CertificatePinner certificatePinner,
                                                         InterceptorConfig interceptor, UnsafeHostnameVerifierConfig unsafeHostnameVerifier,
                                                         RequestTokenInterceptor requestTokenInterceptor) {
        return new OkHttpManagerConfig(unsafeTrustManager, certificatePinner, interceptor, unsafeHostnameVerifier, requestTokenInterceptor);
    }

    @Provides
    @Singleton
    public CertificatePinner providesCertificatePinner() {
        return new CertificatePinner.Builder()

                .build();
    }

    @Provides
    @Singleton
    public UnsafeTrustManagerConfig provideUnsafeCertificateValidator() {
        return new UnsafeTrustManagerConfig();
    }


    @Provides
    @Singleton
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    @Singleton
    public UnsafeHostnameVerifierConfig providesUnsafeHostnameVerifier() {
        return new UnsafeHostnameVerifierConfig();
    }

    @Provides
    @Singleton
    public InterceptorConfig provideInterceptor() {
        return new InterceptorConfig();
    }

    @Provides
    @Singleton
    public RequestTokenInterceptor provideRequestInterceptor() {
        return new RequestTokenInterceptor();
    }

    @Singleton
    @Provides
    public SharedPreferences provideSharedPrefs(Application app) {
        return PreferenceManager.getDefaultSharedPreferences(app);
    }



}
