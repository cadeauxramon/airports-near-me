package com.ramon.airportsnearme.di;

import com.ramon.airportsnearme.landingfragment.LandingFragment;
import com.ramon.airportsnearme.locationdetails.InfoFragment;
import com.ramon.airportsnearme.locationdetails.LocationDetailsFragment;
import com.ramon.airportsnearme.locationdetails.MetarFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Provides the fragments which are going to be injected into by implementing Injectable
 * */
@Module
public abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract LandingFragment contributeLandingFragment();

    @ContributesAndroidInjector
    abstract LocationDetailsFragment contributeDetailsFragment();

    @ContributesAndroidInjector
    abstract InfoFragment contributeInfoFragment();

    @ContributesAndroidInjector
    abstract MetarFragment contributeMetarFragment();

}
