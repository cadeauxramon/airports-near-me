package com.ramon.airportsnearme.locationdetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ramon.airportsnearme.R;
import com.ramon.airportsnearme.databinding.MetarFragmentBinding;
import com.ramon.airportsnearme.di.Injectable;
import com.ramon.airportsnearme.metarviewholder.BottomViewHolder;
import com.ramon.airportsnearme.metarviewholder.CloudViewHolder;
import com.ramon.airportsnearme.metarviewholder.MetarTopViewHolder;
import com.ramon.airportsnearme.webservice.api.decoded.WeatherData;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;


public class MetarFragment extends Fragment implements Injectable {
    @Inject
    Gson gson;

    private MetarFragmentBinding binding;

    public MetarFragment() {
        // Required empty public constructor
    }

    /**
     * puts data into the bundle for safe rotation support
     *
     * @param rawString -raw weather response if it is there
     * @param decodedString- string representation of weather response if it is there
     * @return
     */

    static MetarFragment newInstance(String rawString, String decodedString) {
        MetarFragment fragment = new MetarFragment();
        Bundle args = new Bundle();
        args.putString(LocationDetailsFragment.DECODED_METAR, decodedString);
        args.putString(LocationDetailsFragment.RAW_METAR, rawString);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.metar_fragment, container, false);
        // Inflate the layout for this fragment
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        WeatherData data = LocationDetailsViewModel.getWeatherStation(LocationDetailsViewModel.getDecodedString(getArguments()), gson);
        String raw = LocationDetailsViewModel.getRawString(getArguments());
        //raw may or may not be present but it have weather data or the raw data we should still show it but if we dont have either show error message
        if (null != data || !StringUtils.isEmpty(raw)) {
            binding.setUnknownData(false);
            binding.rv.setAdapter(new MetarAdapter(raw, data));
        } else {
            //error message
            binding.setUnknownData(true);
        }
    }

    /**
     *  class that inflates the Recylcer view
     *  Recycler view was used to account for the fact that multiple cloud conditions may come back from the service
     */
    private class MetarAdapter extends RecyclerView.Adapter {


        private static final int CLOUD_VIEW_HOLDER = 1;
        private static final int TOP_VIEW_HOLDER = 0;
        private static final int BOTTOM_VIEW_HOLDER = 2;
        private final String rawString;
        private final WeatherData weatherdata;

        MetarAdapter(String rawString, WeatherData weatherStation) {
            this.rawString = rawString;
            this.weatherdata = weatherStation;
        }

        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            RecyclerView.ViewHolder viewHolder;
            switch (viewType) {
                case CLOUD_VIEW_HOLDER:
                    viewHolder = CloudViewHolder.inflate(parent);
                    break;
                case BOTTOM_VIEW_HOLDER:
                    viewHolder = BottomViewHolder.inflate(parent);
                    break;
                default:
                    viewHolder = MetarTopViewHolder.inflate(parent);
                    break;

            }

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
            if (holder instanceof MetarTopViewHolder) {
                ((MetarTopViewHolder) holder).bind(rawString, weatherdata);
            } else if (holder instanceof CloudViewHolder) {
                ((CloudViewHolder) holder).bind(weatherdata.getClouds().get(position - 1));
            } else {
                ((BottomViewHolder) holder).bind(weatherdata);
            }

        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return TOP_VIEW_HOLDER;
            } else if (weatherdata != null && !weatherdata.getClouds().isEmpty() && position - 1 < weatherdata.getClouds().size()) {
                return CLOUD_VIEW_HOLDER;
            } else {
                return BOTTOM_VIEW_HOLDER;
            }
        }

        @Override
        public int getItemCount() {
            int itemCount = weatherdata != null && !weatherdata.getClouds().isEmpty() ? weatherdata.getClouds().size() : 0;

            //accounts for cloud items + static top and bottom view holders
            return itemCount + 2;
        }
    }
}
