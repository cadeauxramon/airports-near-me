package com.ramon.airportsnearme.locationdetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.ramon.airportsnearme.R;
import com.ramon.airportsnearme.databinding.FragmentInfoBinding;
import com.ramon.airportsnearme.di.Injectable;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;


public class InfoFragment extends Fragment implements Injectable {

    private static final String STATION_STRING = "STATION_STRING";
    @Inject
    Gson gson;
    private FragmentInfoBinding binding;

     static InfoFragment newInstance(String itemString) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putString(STATION_STRING, itemString);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_info, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        binding.setInfo(LocationDetailsViewModel.getStation(getArguments() != null ? getArguments().getString(STATION_STRING, "") : "", gson));
        binding.executePendingBindings();
    }

}
