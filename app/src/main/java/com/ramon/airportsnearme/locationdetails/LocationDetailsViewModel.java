package com.ramon.airportsnearme.locationdetails;

import android.app.Application;
import android.os.Bundle;

import androidx.lifecycle.AndroidViewModel;

import com.google.gson.Gson;
import com.ramon.airportsnearme.webservice.api.decoded.WeatherData;
import com.ramon.airportsnearme.webservice.api.station.Station;

import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;

public class LocationDetailsViewModel extends AndroidViewModel {

    private final Gson gson;
    private Bundle args;

    @Inject
    public LocationDetailsViewModel(Application application, Gson gson) {
        super(application);
        this.gson = gson;
    }


    /**
     * Converts a json String back into the Station Object from the bundle if the string is found
     *
     * @param convert- Bundle string of json encoded Object
     * @param gson - Gson decoder
     * @return Station Object or null
     */
     static Station getStation(String convert, Gson gson) {
        if (!StringUtils.isEmpty(convert)) {
            return gson.fromJson(convert, Station.class);
        }
        return null;
    }


    /**
     * Converts a json String back into the Station Object from the bundle if the string is found
     *
     * @param convert- Bundle string of json encoded Object
     * @param gson - Gson decoder
     * @return WeatherData Object or null
     */
     static WeatherData getWeatherStation(String convert, Gson gson) {
        if (!StringUtils.isEmpty(convert)) {
            return gson.fromJson(convert, WeatherData.class);
        }
        return null;

    }

    /**
     * Gets the String representation of the object
     *
     * @param args - bundle where data is stored
     * @return Gson encoded String
     */
     static String getItemString(Bundle args) {
        return args != null ? args.getString(LocationDetailsFragment.STATION, "") : "";
    }

    /**
     * Gets the String representation of the object
     *
     * @param args - bundle where data is stored
     * @return Gson encoded String
     */
     static String getRawString(Bundle args) {
        return args != null ? args.getString(LocationDetailsFragment.RAW_METAR, "") : "";
    }

    /**
     * Gets the String representation of the object
     *
     * @param args - bundle where data is stored
     * @return Gson encoded String
     */
     static String getDecodedString(Bundle args) {
        return args != null ? args.getString(LocationDetailsFragment.DECODED_METAR, "") : "";
    }

     Bundle getArgs() {
        return args;
    }

     void setArgs(Bundle args) {
        this.args = args;
    }


}
