package com.ramon.airportsnearme.locationdetails;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

/*
    I recognize that some would have made these fragments inner classes, i prefer to separate anything i expect users to interact with
    into their own java files and layouts, while that may cause extra files to be created, in my experience it makes debugging more manageable

 */
public class LocationDetailsAdapter extends FragmentPagerAdapter {

    /*
        This is kind of a grey area in the community and one that even i haven't been able to really answer
        some would argue that in a strict interpretation of MVVM every fragment and child fragment should have
        its own view model, ultimately think the view model should exist at the highest level fragment but
        would be open to hearing arguments
     */

    private LocationDetailsViewModel viewModel;

     LocationDetailsAdapter(@NonNull FragmentManager fm, int behavior, LocationDetailsViewModel viewModel) {
        super(fm, behavior);
        this.viewModel = viewModel;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;

        if (position == 0) {
            fragment = InfoFragment.newInstance(LocationDetailsViewModel.getItemString(viewModel.getArgs()));
        } else {
            fragment = MetarFragment.newInstance(LocationDetailsViewModel.getRawString(viewModel.getArgs()), LocationDetailsViewModel.getDecodedString(viewModel.getArgs()));

        }


        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        return position == 0 ? "Info" : "Metar";
    }
}
