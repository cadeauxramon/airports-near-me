package com.ramon.airportsnearme.locationdetails;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.ramon.airportsnearme.R;
import com.ramon.airportsnearme.databinding.LocationDetailsFragmentBinding;
import com.ramon.airportsnearme.di.Injectable;

import javax.inject.Inject;

public class LocationDetailsFragment extends Fragment implements Injectable {

    static final String STATION = "station";
    static final String RAW_METAR = "raw_metar";
    static final String DECODED_METAR = "decoded_metar";
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private LocationDetailsViewModel mViewModel;
    private LocationDetailsFragmentBinding binding;


    /**
     * adding args to the bundle so that it is easy to handle rotation and re-inflating static data
     *
     * @param station - string representation of a station
     * @param rawResponse - the raw encoded weather response if it was returned
     * @param decodedWeather -string representation of a WeatherData
     * @return
     */

    public static LocationDetailsFragment newInstance(String station, String rawResponse, String decodedWeather) {
        LocationDetailsFragment fragment = new LocationDetailsFragment();
        Bundle args = new Bundle();
        args.putString(STATION, station);
        args.putString(RAW_METAR, rawResponse);
        args.putString(DECODED_METAR, decodedWeather);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.location_details_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(LocationDetailsViewModel.class);
        mViewModel.setArgs(getArguments());
    }


    @Override
    public void onResume() {
        super.onResume();
        binding.tabs.setupWithViewPager(binding.viewpager);
        binding.viewpager.setAdapter(new LocationDetailsAdapter(getChildFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT, mViewModel));
    }



}
