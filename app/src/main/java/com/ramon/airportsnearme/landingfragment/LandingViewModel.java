package com.ramon.airportsnearme.landingfragment;

import android.app.Activity;
import android.app.Application;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.ramon.airportsnearme.utils.LocationUtils;
import com.ramon.airportsnearme.webservice.api.ApiClient;
import com.ramon.airportsnearme.webservice.api.decoded.WeatherData;
import com.ramon.airportsnearme.webservice.api.station.Station;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

public class LandingViewModel extends AndroidViewModel {
    private final LatLngBounds.Builder latLngBuilder;
    private final ApiClient apiClient;
    private final Application app;
    private final Geocoder geocoder;
    private MutableLiveData<Pair<ApiClient.FinalData, GoogleMap>> mutableLiveData;
    private MutableLiveData<Throwable> errorMutable;
    private FusedLocationProviderClient client;
    private LocationRequest locationRequest;
    private GoogleMap googleMaps;
    private ApiClient.FinalData responseData;
    private boolean googleMapsLoaded;
    private Location savedLastKnownLocation;

    @Inject
    public LandingViewModel(FusedLocationProviderClient client, Application application, ApiClient apiClient) {
        super(application);
        this.app = application;
        this.client = client;

        // request an updated Location after 25 miles displacement
        locationRequest = LocationRequest.create().setSmallestDisplacement(40233.6f);

        latLngBuilder = new LatLngBounds.Builder();
        this.apiClient = apiClient;
        this.geocoder = new Geocoder(application.getBaseContext());
        this.mutableLiveData = new MutableLiveData<>();
        this.errorMutable = new MutableLiveData<>();
    }

    public MutableLiveData<Throwable> getErrorMutable() {
        return errorMutable;
    }

    public void setErrorMutable(MutableLiveData<Throwable> errorMutable) {
        this.errorMutable = errorMutable;
    }

    public MutableLiveData<Pair<ApiClient.FinalData, GoogleMap>> getMutableLiveData() {
        return mutableLiveData;
    }

    public void setMutableLiveData(MutableLiveData<Pair<ApiClient.FinalData, GoogleMap>> mutableLiveData) {
        this.mutableLiveData = mutableLiveData;
    }

    LatLngBounds.Builder getLatLngBuilder() {
        return latLngBuilder;
    }

    FusedLocationProviderClient getClient() {
        return client;
    }

    LocationRequest getLocationRequest() {
        return locationRequest;
    }

    GoogleMap getGoogleMaps() {
        return googleMaps;
    }

    void setGoogleMaps(GoogleMap googleMaps) {
        this.googleMaps = googleMaps;
    }


    /**
     * converts the Markers from the google map instance back into a Station that can use in other areas of the app
     *
     * @param markerLatLng- google map marker
     * @return Station if found if not null
     */
    @Nullable
    Station markerToStation(@NonNull LatLng markerLatLng) {
        for (Station item : responseData.getStationResponse().getData()) {
            if (item.getLatitude().getDecimal() == markerLatLng.latitude && item.getLongitude().getDecimal() == markerLatLng.longitude) {
                return item;
            }
        }
        return null;
    }

    /**
     * @param location- last known location from Fused Location Provider
     * @return Observable thats fetching data
     */
    Observable<ApiClient.FinalData> getData(Location location) {
        Timber.d(location.toString());
        return apiClient.fetchAllNeededData(location != null ? location.getLatitude() : 0.0, location != null ? location.getLongitude() : 0.0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * @param location- last known location from Geocoder
     * @return Observable thats fetching data
     */
    Observable<ApiClient.FinalData> getDataFromAddress(Address location) {
        Timber.d(location.toString());
        return apiClient.fetchAllNeededData(location != null ? location.getLatitude() : 0.0, location != null ? location.getLongitude() : 0.0)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * @param locationInterface- trys to get the last know location to be used
     */
    void getLastKnowAddress(LandingFragment.LocationInterface locationInterface) {
        //attempt to get the latest location

        LocationServices
                .getFusedLocationProviderClient(app)
                .getLastLocation()
                .addOnFailureListener(locationInterface::locationFailed)
                .addOnSuccessListener(locationInterface::locationAcquired);


    }

    ApiClient.FinalData getResponseData() {
        return responseData;
    }

    void setResponseData(ApiClient.FinalData responseData) {
        this.responseData = responseData;
    }

    boolean isMapLoaded() {
        return googleMaps != null && googleMapsLoaded;
    }

    void setMapLoaded(boolean isloaded) {
        this.googleMapsLoaded = isloaded;
    }


    String lookupRawResponse(Station item) {
        //in Kotlin a simple item? and response? would have avoided the need for such aggressive checks
        if (item != null && item.getIcao() != null && responseData.getRawMetarResponse() != null && !responseData.getRawMetarResponse().getData().isEmpty()) {
            //Look to see if the service returned a Raw String for the location
            for (String s : responseData.getRawMetarResponse().getData()) {
                //if the Icao is found in the string that is returned, then return it
                if (s.contains(item.getIcao())) {
                    return s;
                }
            }
        }

        //We either dont have data or nothing was found either way return an empty string
        return "";
    }

    @Nullable
    WeatherData lookupDecodedResponse(Station item) {
        Timber.d("decode : %s", item);
        Timber.d("decoded metar: %s", responseData.getDecodedMetarResponse());
        Timber.d("decoded raw : %s", responseData.getRawMetarResponse());
        //in Kotlin a simple item? and response? would have avoided the need for such aggressive checks
        if (item != null && item.getIcao() != null && responseData != null && responseData.getDecodedMetarResponse() != null && !responseData.getDecodedMetarResponse().getData().isEmpty()) {
            //Look to see if the service returned a Raw String for the location
            for (WeatherData data : responseData.getDecodedMetarResponse().getData()) {
                //if the Icao match then we found our item so return it
                if (data.getIcao().equals(item.getIcao())) {
                    return data;
                }
            }
        }

        //We either dont have data or nothing was found either way return null
        return null;
    }

    @Nullable
    public Address getDefaultAddress() {
        try {
            List<Address> addresses = geocoder.getFromLocationName("Fairfield University", 1);
            return addresses.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveLastKnowLocation(Location location) {
        this.savedLastKnownLocation=location;
    }

    //used for default location search of Airports
    @Nullable
    DisposableObserver<ApiClient.FinalData> fetchDataFromAddress(Address defaultAddress, GoogleMap googleMap) {
        if (defaultAddress != null) {
            return getDataFromAddress(defaultAddress).subscribeWith(new DisposableObserver<ApiClient.FinalData>() {
                @Override
                public void onNext(ApiClient.FinalData finalData) {
                    Timber.d("API RESPONDED %s", finalData.getDecodedMetarResponse());
                    setResponseData(finalData);
                    mutableLiveData.postValue(new Pair<>(finalData, googleMap));

                }

                @Override
                public void onError(Throwable e) {
                    errorMutable.postValue(e);
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
        }
        return null;

    }

    //used for default location search of Airports

    @Nullable
    Disposable fetchData(Location location, GoogleMap googleMap) {
        if (location != null) {
            return getData(location).subscribeWith(new DisposableObserver<ApiClient.FinalData>() {
                @Override
                public void onNext(ApiClient.FinalData finalData) {
                    Timber.d("API RESPONDED %s", finalData.getDecodedMetarResponse());
                    setResponseData(finalData);
                    mutableLiveData.postValue(new Pair<>(finalData, googleMap));


                }

                @Override
                public void onError(Throwable e) {
                    errorMutable.postValue(e);
                    e.printStackTrace();
                }

                @Override
                public void onComplete() {

                }
            });
        }
        return null;
    }

    public void attemptDataRefresh(Activity activity) {
        if(LocationUtils.haveLocationPermissions(activity) && savedLastKnownLocation != null){
            fetchData(savedLastKnownLocation,googleMaps);
        }else {
            fetchDataFromAddress(getDefaultAddress(),googleMaps);
        }

    }
}
