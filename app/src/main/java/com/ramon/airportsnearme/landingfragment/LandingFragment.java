package com.ramon.airportsnearme.landingfragment;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.ramon.airportsnearme.BuildConfig;
import com.ramon.airportsnearme.R;
import com.ramon.airportsnearme.databinding.CustomMapInfoWindowBinding;
import com.ramon.airportsnearme.databinding.LandingFragmentBinding;
import com.ramon.airportsnearme.di.Injectable;
import com.ramon.airportsnearme.locationdetails.LocationDetailsFragment;
import com.ramon.airportsnearme.utils.LocationUtils;
import com.ramon.airportsnearme.utils.Utils;
import com.ramon.airportsnearme.webservice.api.ApiClient;
import com.ramon.airportsnearme.webservice.api.decoded.WeatherData;
import com.ramon.airportsnearme.webservice.api.station.Station;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


/**
 * Fragment that contains the map fragment that will update once we fetch data from an API
 */
public class LandingFragment extends Fragment implements OnMapReadyCallback,
        GoogleMap.InfoWindowAdapter, GoogleMap.OnInfoWindowClickListener,
        GoogleMap.OnMyLocationButtonClickListener, Injectable {

    @Inject
    Gson gson;
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private CompositeDisposable disposable = new CompositeDisposable();
    private LandingFragmentBinding binding;
    private LandingViewModel mViewModel;

    /**
     * Listener that will help us to receive updates as permissions changes (denied access to location then given permission)
     * and as people move around
     */
    private LocationCallback locationCallback = new LocationCallback() {

        @Override
        public void onLocationResult(LocationResult locationResult) {
            //check to make sure the view is available before we try to update it
            if (mViewModel.isMapLoaded()) {
                Disposable call = mViewModel.fetchData(locationResult.getLastLocation(), mViewModel.getGoogleMaps());
                if (call != null) {
                    disposable.add(call);
                }
            }

        }

        @Override
        public void onLocationAvailability(LocationAvailability locationAvailability) {

        }
    };

    public static LandingFragment newInstance() {
        return new LandingFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.landing_fragment, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setIsLoading(true);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        binding.errorView.setFragment(this);
        //check when on resume is called because you might have moved and stale data may need to be moved
        mViewModel.getClient().requestLocationUpdates(mViewModel.getLocationRequest(), locationCallback, Looper.getMainLooper());
        mViewModel.getMutableLiveData().observe(this, dataPair -> {
            binding.setIsLoading(false);
            binding.errorView.setIsError(false);
            populateMarkers(mViewModel.getResponseData(), dataPair.second, mViewModel.getLatLngBuilder());
        });
        mViewModel.getErrorMutable().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                binding.setIsLoading(false);
                binding.errorView.setIsError(true);
            }
        });

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(LandingViewModel.class);
    }

    public void attemptDataRefresh() {
        binding.errorView.setIsError(false);
        mViewModel.attemptDataRefresh(getActivity());
    }

    @Override
    public View getInfoWindow(Marker marker) {
        Station selected = mViewModel.markerToStation(marker.getPosition());
        CustomMapInfoWindowBinding view = DataBindingUtil.bind(LayoutInflater.from(getContext()).inflate(R.layout.custom_map_info_window, null, false));
        if (selected != null && view != null) {
            view.setStation(selected);
            view.executePendingBindings();
            return view.getRoot();
        }

        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        final Station item = mViewModel.markerToStation(marker.getPosition());
        final String rawResponse = mViewModel.lookupRawResponse(item);
        final WeatherData decodedResponse = mViewModel.lookupDecodedResponse(item);

        if (null != item && getActivity() != null) {
            LocationDetailsFragment fragment = LocationDetailsFragment.newInstance(gson.toJson(item), rawResponse, gson.toJson(decodedResponse));
            getActivity()
                    .getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .addToBackStack(fragment.getTag())
                    .commit();
        }
    }

    private void populateMarkers(ApiClient.FinalData results, GoogleMap googleMap, LatLngBounds.Builder latLngBuilder) {
        try {
            // Add marker on the map for each search result
            for (Station item : results.getStationResponse().getData()) {
                // we will consider an office with default values (0) for lat/long
                // to have an invalid location and not display it
                if (0.0 != item.getLatitude().getDecimal() && 0.0 != item.getLongitude().getDecimal()) {
                    LatLng itemLatLng = new LatLng(item.getLatitude().getDecimal(), item.getLongitude().getDecimal());
                    latLngBuilder.include(itemLatLng);
                    if (googleMap != null) {
                        googleMap.addMarker(new MarkerOptions()
                                .position(itemLatLng)
                                .icon(BitmapDescriptorFactory.fromBitmap(Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.map_pin)))
                                // using the xxxhdpi image as a reference the bottom of the pin appears to be at 48,120 of a 96x248px image
                                .anchor(48 / 96f, 120 / 248f));
                    }
                }
            }

            binding.setIsLoading(false);

            if (googleMap != null) {
                // Move the camera to view all the pins, and set the custom info window adapter
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(latLngBuilder.build(), 100));
                googleMap.setInfoWindowAdapter(this);
                googleMap.setOnInfoWindowClickListener(this);
            }
        } catch (NullPointerException e) {
            if (BuildConfig.LOGGING_ENABLED) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mViewModel.setGoogleMaps(googleMap);

        if (getActivity() != null && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            googleMap.setMyLocationEnabled(true);
        }

        googleMap.setOnMapLoadedCallback(() -> {
            //again make sure the map is fully loaded we try to manipulate it
            mViewModel.setMapLoaded(true);
            //do a permission check and then do some default action if that fails
            if (LocationUtils.haveLocationPermissions(getActivity())) {
                mViewModel.getLastKnowAddress(new LocationInterface() {
                                                  @Override
                                                  public void locationAcquired(Location location) {
                                                      mViewModel.saveLastKnowLocation(location);
                                                      mViewModel.fetchData(location, googleMap);
                                                  }

                                                  @Override
                                                  public void locationFailed(Exception e) {
                                                      if (BuildConfig.LOGGING_ENABLED) {
                                                          e.printStackTrace();
                                                      }
                                                      mViewModel.fetchDataFromAddress(mViewModel.getDefaultAddress(), googleMap);
                                                  }
                                              }
                );
            } else {
                Toast.makeText(getContext(), "Setting Default Location To Fairfield University", Toast.LENGTH_LONG).show();
                placeDefaultIcon(googleMap, mViewModel.getDefaultAddress(), mViewModel.getLatLngBuilder());
                mViewModel.fetchDataFromAddress(mViewModel.getDefaultAddress(), googleMap);
            }

        });


    }

    //places a red android guy to denote default location if we for some reason have no data
    private void placeDefaultIcon(GoogleMap googleMap, Address defaultAddress, LatLngBounds.Builder latLngBuilder) {
        if (defaultAddress != null) {
            LatLng itemLatLng = new LatLng(defaultAddress.getLatitude(), defaultAddress.getLongitude());
            latLngBuilder.include(itemLatLng);
            if (googleMap != null) {
                googleMap.addMarker(new MarkerOptions()
                        .position(itemLatLng)
                        .icon(BitmapDescriptorFactory.fromBitmap(Utils.getBitmapFromVectorDrawable(getContext(), R.drawable.default_loc_marker)))
                        // using the xxxhdpi image as a reference the bottom of the pin appears to be at 48,120 of a 96x248px image
                        .anchor(48 / 96f, 120 / 248f));
            }
        }

    }


    // Originally considered doing some custom action if you clicked the my location button but become less needed as I tested
    @Override
    public boolean onMyLocationButtonClick() {
        return true;
    }

    @Override
    public void onPause() {
        super.onPause();
        mViewModel.setGoogleMaps(null);
        if (disposable != null && !disposable.isDisposed()) disposable.dispose();
    }

    public interface LocationInterface {
        void locationAcquired(Location location);

        void locationFailed(Exception e);
    }
}
