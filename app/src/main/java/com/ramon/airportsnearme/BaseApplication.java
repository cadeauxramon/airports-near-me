package com.ramon.airportsnearme;

import android.app.Activity;
import android.app.Application;


import com.ramon.airportsnearme.BuildConfig;
import com.ramon.airportsnearme.di.AppInjector;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;

public class BaseApplication extends Application implements HasActivityInjector {
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        AppInjector.init(this);
        initTimber();
    }

    private void initTimber() {
        if (BuildConfig.LOGGING_ENABLED) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

}
