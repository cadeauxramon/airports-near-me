
package com.ramon.airportsnearme.webservice.api.decoded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Barometer {

    @SerializedName("hg")
    @Expose
    private Double hg;
    @SerializedName("hpa")
    @Expose
    private Double hpa;
    @SerializedName("kpa")
    @Expose
    private Double kpa;
    @SerializedName("mb")
    @Expose
    private Double mb;

    public Double getHg() {
        return hg;
    }

    public void setHg(Double hg) {
        this.hg = hg;
    }

    public Double getHpa() {
        return hpa;
    }

    public void setHpa(Double hpa) {
        this.hpa = hpa;
    }

    public Double getKpa() {
        return kpa;
    }

    public void setKpa(Double kpa) {
        this.kpa = kpa;
    }

    public Double getMb() {
        return mb;
    }

    public void setMb(Double mb) {
        this.mb = mb;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("hg", hg).append("hpa", hpa).append("kpa", kpa).append("mb", mb).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(kpa).append(mb).append(hg).append(hpa).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Barometer) == false) {
            return false;
        }
        Barometer rhs = ((Barometer) other);
        return new EqualsBuilder().append(kpa, rhs.kpa).append(mb, rhs.mb).append(hg, rhs.hg).append(hpa, rhs.hpa).isEquals();
    }

}
