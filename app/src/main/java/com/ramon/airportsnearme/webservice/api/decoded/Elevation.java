
package com.ramon.airportsnearme.webservice.api.decoded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Elevation {

    @SerializedName("feet")
    @Expose
    private Double feet;
    @SerializedName("meters")
    @Expose
    private Double meters;

    public Double getFeet() {
        return feet;
    }

    public void setFeet(Double feet) {
        this.feet = feet;
    }

    public Double getMeters() {
        return meters;
    }

    public void setMeters(Double meters) {
        this.meters = meters;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("feet", feet).append("meters", meters).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(meters).append(feet).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Elevation) == false) {
            return false;
        }
        Elevation rhs = ((Elevation) other);
        return new EqualsBuilder().append(meters, rhs.meters).append(feet, rhs.feet).isEquals();
    }

}
