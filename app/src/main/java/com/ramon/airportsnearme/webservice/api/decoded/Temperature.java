
package com.ramon.airportsnearme.webservice.api.decoded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Temperature {

    @SerializedName("celsius")
    @Expose
    private Double celsius;
    @SerializedName("fahrenheit")
    @Expose
    private Double fahrenheit;

    public Double getCelsius() {
        return celsius;
    }

    public void setCelsius(Double celsius) {
        this.celsius = celsius;
    }

    public Double getFahrenheit() {
        return fahrenheit;
    }

    public void setFahrenheit(Double fahrenheit) {
        this.fahrenheit = fahrenheit;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("celsius", celsius).append("fahrenheit", fahrenheit).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(celsius).append(fahrenheit).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Temperature) == false) {
            return false;
        }
        Temperature rhs = ((Temperature) other);
        return new EqualsBuilder().append(celsius, rhs.celsius).append(fahrenheit, rhs.fahrenheit).isEquals();
    }

}
