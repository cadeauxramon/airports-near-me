
package com.ramon.airportsnearme.webservice.api.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Radius {

    @SerializedName("from")
    @Expose
    private From from;
    @SerializedName("miles")
    @Expose
    private Double miles;
    @SerializedName("meters")
    @Expose
    private Double meters;
    @SerializedName("bearing")
    @Expose
    private Double bearing;
    @SerializedName("direction")
    @Expose
    private String direction;

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public Double getMiles() {
        return miles;
    }

    public void setMiles(Double miles) {
        this.miles = miles;
    }

    public Double getMeters() {
        return meters;
    }

    public void setMeters(Double meters) {
        this.meters = meters;
    }

    public Double getBearing() {
        return bearing;
    }

    public void setBearing(Double bearing) {
        this.bearing = bearing;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("from", from).append("miles", miles).append("meters", meters).append("bearing", bearing).append("direction", direction).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(meters).append(direction).append(bearing).append(miles).append(from).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Radius) == false) {
            return false;
        }
        Radius rhs = ((Radius) other);
        return new EqualsBuilder().append(meters, rhs.meters).append(direction, rhs.direction).append(bearing, rhs.bearing).append(miles, rhs.miles).append(from, rhs.from).isEquals();
    }

}
