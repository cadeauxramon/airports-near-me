
package com.ramon.airportsnearme.webservice.api.decoded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Visibility {

    @SerializedName("miles")
    @Expose
    private String miles;
    @SerializedName("miles_float")
    @Expose
    private Double milesFloat;
    @SerializedName("meters")
    @Expose
    private String meters;
    @SerializedName("meters_float")
    @Expose
    private Double metersFloat;

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public Double getMilesFloat() {
        return milesFloat;
    }

    public void setMilesFloat(Double milesFloat) {
        this.milesFloat = milesFloat;
    }

    public String getMeters() {
        return meters;
    }

    public void setMeters(String meters) {
        this.meters = meters;
    }

    public Double getMetersFloat() {
        return metersFloat;
    }

    public void setMetersFloat(Double metersFloat) {
        this.metersFloat = metersFloat;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("miles", miles).append("milesFloat", milesFloat).append("meters", meters).append("metersFloat", metersFloat).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(milesFloat).append(meters).append(metersFloat).append(miles).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Visibility) == false) {
            return false;
        }
        Visibility rhs = ((Visibility) other);
        return new EqualsBuilder().append(milesFloat, rhs.milesFloat).append(meters, rhs.meters).append(metersFloat, rhs.metersFloat).append(miles, rhs.miles).isEquals();
    }

}
