package com.ramon.airportsnearme.webservice.api.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

import timber.log.Timber;

public class StationResponse {

    @SerializedName("results")
    @Expose
    private Integer results;
    @SerializedName("data")
    @Expose
    private List<Station> data = null;

    public Integer getResults() {
        return results;
    }

    public void setResults(Integer results) {
        this.results = results;
    }

    public List<Station> getData() {
        return data;
    }

    public void setData(List<Station> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("results", results).append("data", data).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(results).append(data).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof StationResponse) == false) {
            return false;
        }
        StationResponse rhs = ((StationResponse) other);
        return new EqualsBuilder().append(results, rhs.results).append(data, rhs.data).isEquals();
    }
}
