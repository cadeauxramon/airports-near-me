
package com.ramon.airportsnearme.webservice.api.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Timezone {

    @SerializedName("gmt")
    @Expose
    private Integer gmt;
    @SerializedName("dst")
    @Expose
    private Integer dst;
    @SerializedName("tzid")
    @Expose
    private String tzid;

    public Integer getGmt() {
        return gmt;
    }

    public void setGmt(Integer gmt) {
        this.gmt = gmt;
    }

    public Integer getDst() {
        return dst;
    }

    public void setDst(Integer dst) {
        this.dst = dst;
    }

    public String getTzid() {
        return tzid;
    }

    public void setTzid(String tzid) {
        this.tzid = tzid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("gmt", gmt).append("dst", dst).append("tzid", tzid).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(gmt).append(dst).append(tzid).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Timezone) == false) {
            return false;
        }
        Timezone rhs = ((Timezone) other);
        return new EqualsBuilder().append(gmt, rhs.gmt).append(dst, rhs.dst).append(tzid, rhs.tzid).isEquals();
    }

}
