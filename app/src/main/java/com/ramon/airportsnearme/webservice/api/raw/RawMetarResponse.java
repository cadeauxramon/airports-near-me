package com.ramon.airportsnearme.webservice.api.raw;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class RawMetarResponse {

    @SerializedName("results")
    @Expose
    private Integer results;
    @SerializedName("data")
    @Expose
    private List<String> data = null;

    public Integer getResults() {
        return results;
    }

    public void setResults(Integer results) {
        this.results = results;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("results", results).append("data", data).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(results).append(data).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RawMetarResponse) == false) {
            return false;
        }
        RawMetarResponse rhs = ((RawMetarResponse) other);
        return new EqualsBuilder().append(results, rhs.results).append(data, rhs.data).isEquals();
    }

}