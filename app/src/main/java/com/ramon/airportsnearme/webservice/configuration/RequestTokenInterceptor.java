package com.ramon.airportsnearme.webservice.configuration;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestTokenInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request= chain.request();

        request= request. newBuilder()
                .addHeader("X-API-Key","2599d6b6fc6d58fd58db955159")
                .build();

        return chain.proceed(request);
    }
}
