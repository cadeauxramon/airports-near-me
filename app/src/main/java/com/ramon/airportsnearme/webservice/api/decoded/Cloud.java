
package com.ramon.airportsnearme.webservice.api.decoded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Cloud {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("base_feet_agl")
    @Expose
    private Double baseFeetAgl;
    @SerializedName("base_meters_agl")
    @Expose
    private Double baseMetersAgl;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Double getBaseFeetAgl() {
        return baseFeetAgl;
    }

    public void setBaseFeetAgl(Double baseFeetAgl) {
        this.baseFeetAgl = baseFeetAgl;
    }

    public Double getBaseMetersAgl() {
        return baseMetersAgl;
    }

    public void setBaseMetersAgl(Double baseMetersAgl) {
        this.baseMetersAgl = baseMetersAgl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("code", code).append("text", text).append("baseFeetAgl", baseFeetAgl).append("baseMetersAgl", baseMetersAgl).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(text).append(baseFeetAgl).append(baseMetersAgl).append(code).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Cloud) == false) {
            return false;
        }
        Cloud rhs = ((Cloud) other);
        return new EqualsBuilder().append(text, rhs.text).append(baseFeetAgl, rhs.baseFeetAgl).append(baseMetersAgl, rhs.baseMetersAgl).append(code, rhs.code).isEquals();
    }

}
