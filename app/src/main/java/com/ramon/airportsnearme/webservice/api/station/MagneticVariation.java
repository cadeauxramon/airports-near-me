
package com.ramon.airportsnearme.webservice.api.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class MagneticVariation {

    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("year")
    @Expose
    private Integer year;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("position", position).append("year", year).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(position).append(year).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof MagneticVariation) == false) {
            return false;
        }
        MagneticVariation rhs = ((MagneticVariation) other);
        return new EqualsBuilder().append(position, rhs.position).append(year, rhs.year).isEquals();
    }

}
