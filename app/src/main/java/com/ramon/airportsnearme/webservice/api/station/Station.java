
package com.ramon.airportsnearme.webservice.api.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Station {

    @SerializedName("state")
    @Expose
    private State state;
    @SerializedName("country")
    @Expose
    private Country country;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("timezone")
    @Expose
    private Timezone timezone;
    @SerializedName("elevation")
    @Expose
    private Elevation elevation;
    @SerializedName("radius")
    @Expose
    private Radius radius;
    @SerializedName("icao")
    @Expose
    private String icao;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("latitude")
    @Expose
    private Latitude latitude;
    @SerializedName("longitude")
    @Expose
    private Longitude longitude;
    @SerializedName("magnetic_variation")
    @Expose
    private MagneticVariation magneticVariation;
    @SerializedName("iata")
    @Expose
    private String iata;
    @SerializedName("useage")
    @Expose
    private String useage;
    @SerializedName("sectional")
    @Expose
    private String sectional;
    @SerializedName("activated")
    @Expose
    private String activated;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Timezone getTimezone() {
        return timezone;
    }

    public void setTimezone(Timezone timezone) {
        this.timezone = timezone;
    }

    public Elevation getElevation() {
        return elevation;
    }

    public void setElevation(Elevation elevation) {
        this.elevation = elevation;
    }

    public Radius getRadius() {
        return radius;
    }

    public void setRadius(Radius radius) {
        this.radius = radius;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Latitude getLatitude() {
        return latitude;
    }

    public void setLatitude(Latitude latitude) {
        this.latitude = latitude;
    }

    public Longitude getLongitude() {
        return longitude;
    }

    public void setLongitude(Longitude longitude) {
        this.longitude = longitude;
    }

    public MagneticVariation getMagneticVariation() {
        return magneticVariation;
    }

    public void setMagneticVariation(MagneticVariation magneticVariation) {
        this.magneticVariation = magneticVariation;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getUseage() {
        return useage;
    }

    public void setUseage(String useage) {
        this.useage = useage;
    }

    public String getSectional() {
        return sectional;
    }

    public void setSectional(String sectional) {
        this.sectional = sectional;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("state", state).append("country", country).append("location", location).append("timezone", timezone).append("elevation", elevation).append("radius", radius).append("icao", icao).append("name", name).append("type", type).append("city", city).append("status", status).append("latitude", latitude).append("longitude", longitude).append("magneticVariation", magneticVariation).append("iata", iata).append("useage", useage).append("sectional", sectional).append("activated", activated).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(status).append(location).append(elevation).append(useage).append(state).append(magneticVariation).append(type).append(city).append(country).append(iata).append(timezone).append(icao).append(activated).append(name).append(longitude).append(radius).append(latitude).append(sectional).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Station) == false) {
            return false;
        }
        Station rhs = ((Station) other);
        return new EqualsBuilder().append(status, rhs.status).append(location, rhs.location).append(elevation, rhs.elevation).append(useage, rhs.useage).append(state, rhs.state).append(magneticVariation, rhs.magneticVariation).append(type, rhs.type).append(city, rhs.city).append(country, rhs.country).append(iata, rhs.iata).append(timezone, rhs.timezone).append(icao, rhs.icao).append(activated, rhs.activated).append(name, rhs.name).append(longitude, rhs.longitude).append(radius, rhs.radius).append(latitude, rhs.latitude).append(sectional, rhs.sectional).isEquals();
    }

}
