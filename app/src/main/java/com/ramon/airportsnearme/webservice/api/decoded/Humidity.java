
package com.ramon.airportsnearme.webservice.api.decoded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Humidity {

    @SerializedName("percent")
    @Expose
    private Double percent;

    public Double getPercent() {
        return percent;
    }

    public void setPercent(Double percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("percent", percent).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(percent).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Humidity) == false) {
            return false;
        }
        Humidity rhs = ((Humidity) other);
        return new EqualsBuilder().append(percent, rhs.percent).isEquals();
    }

}
