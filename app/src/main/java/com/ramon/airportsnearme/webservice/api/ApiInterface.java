package com.ramon.airportsnearme.webservice.api;


import com.ramon.airportsnearme.webservice.api.decoded.DecodedMetarResponse;
import com.ramon.airportsnearme.webservice.api.raw.RawMetarResponse;
import com.ramon.airportsnearme.webservice.api.station.StationResponse;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("station/lat/{lat_address}/lon/{lng_address}/radius/{radius_address}")
    Observable<StationResponse> getAirportsNearLatLng(@Path("lat_address") Double lat,
                                                      @Path("lng_address") Double lng,
                                                      @Path("radius_address") Double radius,
                                                      @Query("filter") String filter);


    @GET("/metar/{ico_codes}")
    Observable<RawMetarResponse> getRawMetarData(@Path("ico_codes") String codes);


    @GET("/metar/{ico_codes}/decoded")
    Observable<DecodedMetarResponse> getDecodedMetarData(@Path("ico_codes") String codes);

}
