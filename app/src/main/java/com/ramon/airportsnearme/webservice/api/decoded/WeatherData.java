
package com.ramon.airportsnearme.webservice.api.decoded;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class WeatherData {

    @SerializedName("wind")
    @Expose
    private Wind wind;
    @SerializedName("temperature")
    @Expose
    private Temperature temperature;
    @SerializedName("dewpoint")
    @Expose
    private Dewpoint dewpoint;
    @SerializedName("humidity")
    @Expose
    private Humidity humidity;
    @SerializedName("barometer")
    @Expose
    private Barometer barometer;
    @SerializedName("visibility")
    @Expose
    private Visibility visibility;
    @SerializedName("elevation")
    @Expose
    private Elevation elevation;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("icao")
    @Expose
    private String icao;
    @SerializedName("observed")
    @Expose
    private String observed;
    @SerializedName("raw_text")
    @Expose
    private String rawText;
    @SerializedName("station")
    @Expose
    private Station station;
    @SerializedName("clouds")
    @Expose
    private List<Cloud> clouds = null;
    @SerializedName("flight_category")
    @Expose
    private String flightCategory;
    @SerializedName("conditions")
    @Expose
    private List<Object> conditions = null;

    public Wind getWind() {
        return wind;
    }

    public void setWind(Wind wind) {
        this.wind = wind;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }

    public Dewpoint getDewpoint() {
        return dewpoint;
    }

    public void setDewpoint(Dewpoint dewpoint) {
        this.dewpoint = dewpoint;
    }

    public Humidity getHumidity() {
        return humidity;
    }

    public void setHumidity(Humidity humidity) {
        this.humidity = humidity;
    }

    public Barometer getBarometer() {
        return barometer;
    }

    public void setBarometer(Barometer barometer) {
        this.barometer = barometer;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public void setVisibility(Visibility visibility) {
        this.visibility = visibility;
    }

    public Elevation getElevation() {
        return elevation;
    }

    public void setElevation(Elevation elevation) {
        this.elevation = elevation;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public String getObserved() {
        return observed;
    }

    public void setObserved(String observed) {
        this.observed = observed;
    }

    public String getRawText() {
        return rawText;
    }

    public void setRawText(String rawText) {
        this.rawText = rawText;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public List<Cloud> getClouds() {
        return clouds;
    }

    public void setClouds(List<Cloud> clouds) {
        this.clouds = clouds;
    }

    public String getFlightCategory() {
        return flightCategory;
    }

    public void setFlightCategory(String flightCategory) {
        this.flightCategory = flightCategory;
    }

    public List<Object> getConditions() {
        return conditions;
    }

    public void setConditions(List<Object> conditions) {
        this.conditions = conditions;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("wind", wind).append("temperature", temperature).append("dewpoint", dewpoint).append("humidity", humidity).append("barometer", barometer).append("visibility", visibility).append("elevation", elevation).append("location", location).append("icao", icao).append("observed", observed).append("rawText", rawText).append("station", station).append("clouds", clouds).append("flightCategory", flightCategory).append("conditions", conditions).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(clouds).append(rawText).append(wind).append(flightCategory).append(station).append(location).append(visibility).append(elevation).append(temperature).append(conditions).append(observed).append(humidity).append(icao).append(dewpoint).append(barometer).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof WeatherData) == false) {
            return false;
        }
        WeatherData rhs = ((WeatherData) other);
        return new EqualsBuilder().append(clouds, rhs.clouds).append(rawText, rhs.rawText).append(wind, rhs.wind).append(flightCategory, rhs.flightCategory).append(station, rhs.station).append(location, rhs.location).append(visibility, rhs.visibility).append(elevation, rhs.elevation).append(temperature, rhs.temperature).append(conditions, rhs.conditions).append(observed, rhs.observed).append(humidity, rhs.humidity).append(icao, rhs.icao).append(dewpoint, rhs.dewpoint).append(barometer, rhs.barometer).isEquals();
    }

}
