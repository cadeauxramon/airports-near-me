package com.ramon.airportsnearme.webservice.api.decoded;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Wind {

    @SerializedName("degrees")
    @Expose
    private Double degrees;
    @SerializedName("speed_kts")
    @Expose
    private Double speedKts;
    @SerializedName("speed_mph")
    @Expose
    private Double speedMph;
    @SerializedName("speed_mps")
    @Expose
    private Double speedMps;
    @SerializedName("gust_kts")
    @Expose
    private Double gustkts;
    @SerializedName("gust_mph")
    @Expose
    private Double gustMph;
    @SerializedName("gust_mps")
    @Expose
    private Double gustMps;

    public Double getGustkts() {
        return gustkts;
    }

    public void setGustkts(Double gustkts) {
        this.gustkts = gustkts;
    }

    public Double getGustMph() {
        return gustMph;
    }

    public void setGustMph(Double gustMph) {
        this.gustMph = gustMph;
    }

    public Double getGustMps() {
        return gustMps;
    }

    public void setGustMps(Double gustMps) {
        this.gustMps = gustMps;
    }

    public Double getDegrees() {
        return degrees;
    }

    public void setDegrees(Double degrees) {
        this.degrees = degrees;
    }

    public Double getSpeedKts() {
        return speedKts;
    }

    public void setSpeedKts(Double speedKts) {
        this.speedKts = speedKts;
    }

    public Double getSpeedMph() {
        return speedMph;
    }

    public void setSpeedMph(Double speedMph) {
        this.speedMph = speedMph;
    }

    public Double getSpeedMps() {
        return speedMps;
    }

    public void setSpeedMps(Double speedMps) {
        this.speedMps = speedMps;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("degrees", degrees).append("speedKts", speedKts).append("speedMph", speedMph).append("speedMps", speedMps).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(speedKts).append(speedMps).append(degrees).append(speedMph).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Wind) == false) {
            return false;
        }
        Wind rhs = ((Wind) other);
        return new EqualsBuilder().append(speedKts, rhs.speedKts).append(speedMps, rhs.speedMps).append(degrees, rhs.degrees).append(speedMph, rhs.speedMph).isEquals();
    }

}
