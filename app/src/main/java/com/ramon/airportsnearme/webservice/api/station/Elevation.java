
package com.ramon.airportsnearme.webservice.api.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Elevation {

    @SerializedName("feet")
    @Expose
    private Double feet;
    @SerializedName("meters")
    @Expose
    private Double meters;
    @SerializedName("method")
    @Expose
    private String method;

    public Double getFeet() {
        return feet;
    }

    public void setFeet(Double feet) {
        this.feet = feet;
    }

    public Double getMeters() {
        return meters;
    }

    public void setMeters(Double meters) {
        this.meters = meters;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("feet", feet).append("meters", meters).append("method", method).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(meters).append(feet).append(method).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Elevation) == false) {
            return false;
        }
        Elevation rhs = ((Elevation) other);
        return new EqualsBuilder().append(meters, rhs.meters).append(feet, rhs.feet).append(method, rhs.method).isEquals();
    }

}
