package com.ramon.airportsnearme.webservice.configuration;

import android.annotation.SuppressLint;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/*
* This class is used for development purposes only. I use Charles Proxy to inspect request in flight
* and the certificate causes HTTP errors this is disabled once a release build is generated
*/

public class UnsafeHostnameVerifierConfig implements HostnameVerifier {
    @SuppressLint("BadHostnameVerifier")
    @Override
    public boolean verify(String hostname, SSLSession session) {
        return true;
    }
}