package com.ramon.airportsnearme.webservice.api;

import android.app.Application;

import com.ramon.airportsnearme.webservice.api.decoded.DecodedMetarResponse;
import com.ramon.airportsnearme.webservice.api.raw.RawMetarResponse;
import com.ramon.airportsnearme.webservice.api.station.Station;
import com.ramon.airportsnearme.webservice.api.station.StationResponse;

import io.reactivex.Observable;
import timber.log.Timber;

public class ApiClient {
    private static final String FILTER = "A";
    private static final double RADIUS = 25;
    private final Application app;
    private final ApiInterface client;

    public ApiClient(Application app, ApiInterface client) {
        this.app = app;
        this.client = client;
    }


    private Observable<StationResponse> fetchStation(double lat, double lng) {
        return client.getAirportsNearLatLng(lat, lng, RADIUS, FILTER);
    }

    private Observable<DecodedMetarResponse> fetchDecodedData(String icaoCodes) {
        return client.getDecodedMetarData(icaoCodes).doOnError(Throwable::printStackTrace);
    }

    private Observable<RawMetarResponse> fetchRawData(String icaoCodes) {
        Timber.d("icao: %s",icaoCodes);
        return client.getRawMetarData(icaoCodes).doOnError(Throwable::printStackTrace);
    }

    public Observable<FinalData> fetchAllNeededData(double lat, double lng) {
        return fetchStation(lat, lng)
                .flatMap(stationResponse ->{
                    Timber.d("in flat Map %s", stationResponse);
                    Timber.d("in flat Map %s", getIcaoCodes(stationResponse));
                        return Observable.zip(Observable.just(stationResponse),
                                fetchDecodedData(getIcaoCodes(stationResponse)),
                                fetchRawData(getIcaoCodes(stationResponse)),
                                FinalData::new);
                });

    }

    public static class FinalData {
        StationResponse stationResponse;
        DecodedMetarResponse decodedMetarResponse;
        RawMetarResponse rawMetarResponse;

        public FinalData(StationResponse stationResponse, DecodedMetarResponse decodedMetarResponse, RawMetarResponse rawMetarResponse) {
            this.stationResponse = stationResponse;
            this.decodedMetarResponse = decodedMetarResponse;
            this.rawMetarResponse = rawMetarResponse;
        }

        public StationResponse getStationResponse() {
            return stationResponse;
        }

        public void setStationResponse(StationResponse stationResponse) {
            this.stationResponse = stationResponse;
        }

        public DecodedMetarResponse getDecodedMetarResponse() {
            return decodedMetarResponse;
        }

        public void setDecodedMetarResponse(DecodedMetarResponse decodedMetarResponse) {
            this.decodedMetarResponse = decodedMetarResponse;
        }

        public RawMetarResponse getRawMetarResponse() {
            return rawMetarResponse;
        }

        public void setRawMetarResponse(RawMetarResponse rawMetarResponse) {
            this.rawMetarResponse = rawMetarResponse;
        }
    }

    public String getIcaoCodes(StationResponse stationResponse) {
        String resultString = "";
        if (stationResponse.getData() != null && !stationResponse.getData().isEmpty()) {
            StringBuilder result = new StringBuilder();
            for (Station datum : stationResponse.getData()) {
                result.append(datum.getIcao());
                result.append(",");
            }
            resultString = result.substring(0, result.toString().length() - 1);
        }
        Timber.d("getIcaoCodes %s", resultString);
        return resultString;
    }

}
