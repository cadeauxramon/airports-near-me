
package com.ramon.airportsnearme.webservice.api.station;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class Latitude {

    @SerializedName("decimal")
    @Expose
    private Double decimal;
    @SerializedName("degrees")
    @Expose
    private String degrees;

    public Double getDecimal() {
        return decimal;
    }

    public void setDecimal(Double decimal) {
        this.decimal = decimal;
    }

    public String getDegrees() {
        return degrees;
    }

    public void setDegrees(String degrees) {
        this.degrees = degrees;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("decimal", decimal).append("degrees", degrees).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(degrees).append(decimal).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Latitude) == false) {
            return false;
        }
        Latitude rhs = ((Latitude) other);
        return new EqualsBuilder().append(degrees, rhs.degrees).append(decimal, rhs.decimal).isEquals();
    }

}
